'use strict';

const express = require('express');
const bodyParser = require('body-parser');

const config = require('./lib/config');

const app = express();

app.use(bodyParser.json({ limit: '50mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

/* ------- HTTPS ------- */

const https = require('https');
const options = config.httpsOptions;

https.createServer(options, app).listen(config.port, function () {

    console.log('HTTPS server listening on port ' + config.port + "...");

    // 1) Connect to BBDD
    require('./lib/connectMongoose');

    // 2.1) Routes
    const index = require('./routes/index');
    const bills = require('./routes/api/bills');
    const camps = require('./routes/api/camps');
    const charts = require('./routes/api/charts');
    const login = require('./routes/api/login');
    const messages = require('./routes/api/messages');
    const programmings = require('./routes/api/programmings');
    const users = require('./routes/api/users');

    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Origin, X-Requested-With, Accept');
        res.header('Access-Control-Allow-Origin', '*');
        //res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
        res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
        //res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
        next();
    });

    // 2.2) Routes
    app.use('/ecamp', index);
    app.use('/ecamp/api/bills', bills);
    app.use('/ecamp/api/camps', camps);
    app.use('/ecamp/api/charts', charts);
    app.use('/ecamp/api/login', login);
    app.use('/ecamp/api/messages', messages);
    app.use('/ecamp/api/programmings', programmings);
    app.use('/ecamp/api/users', users);
});

/* ------- HTTPS ------- */

module.exports = app;