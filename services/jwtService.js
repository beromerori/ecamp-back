'use strict';

const jwt = require('jsonwebtoken');
const moment = require('moment');

const config = require('../lib/config');
const COMMON = require('../common/constants');

function createJWT(user) {

    var payload = {
        sub: user.role,
        iat: moment().unix(),
        exp: moment().add(1, 'days').unix()
    }

    return jwt.sign(payload, config.secretJWT);
}

function decodeJWT(token) {

    return new Promise(function (resolve, reject) {

        try {

            const decodedToken = jwt.decode(token);

            if (decodedToken && (decodedToken.exp < moment().unix())) {
                reject({ status: 409, message: COMMON.SERVER.ERRORS.AUTHORIZATION.TOKEN_EXPIRED });
            }

            const payload = jwt.verify(token, config.secretJWT);
            resolve(payload.sub);
        }
        catch (error) {
            reject({ status: 503, message: COMMON.SERVER.ERRORS.AUTHORIZATION.TOKEN_INVALID });
        }
    });
}

module.exports = {
    createJWT,
    decodeJWT
}
