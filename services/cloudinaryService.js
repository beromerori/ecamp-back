'use strict';

const config = require('../lib/config');
const cloudinary = require('cloudinary');
cloudinary.config(config.cloudinary);

const COMMON = require('../common/constants');

function uploadImage(code, image) {

    return new Promise(function (resolve, reject) {

        try {

            cloudinary.v2.uploader.upload(image, { folder: `ecamp/camps/${code}` },
                function (error, result) {

                    if (error) reject({ status: 500, message: COMMON.SERVER.ERRORS.MEDIA.NOT_MEDIA });

                    resolve({ status: 200, url: result.url });
                }
            );
        }
        catch (error) {
            reject({ status: 500, message: COMMON.SERVER.ERRORS.MEDIA.NOT_MEDIA });
        }
    });
}

module.exports = {
    uploadImage
}