'use strict';

const express = require('express');
const router = express.Router();

// My controllers
const applicationCtrl = require('../../controllers/applications');
const campCtrl = require('../../controllers/camps');
const mediaCtrl = require('../../controllers/media');

// My middlewares
const auth = require('../../middlewares/auth');

/** ---------- GET ---------- */
router.get('/', campCtrl.getCamps);
router.get('/:id', campCtrl.getCamp);

/** ---------- POST ---------- */
router.post('/', auth.isSuper, campCtrl.createCamp);
router.post('/:id/applications', applicationCtrl.createApplication);
router.post('/media/files', auth.isMedium, mediaCtrl.uploadFile);

/** ---------- PUT ---------- */
router.put('/:id', auth.isSuper, campCtrl.updateCamp);

/** ---------- DELETE ---------- */
router.delete('/:id', auth.isSuper, campCtrl.deleteCamp);
router.delete('/media/files/:public_id', auth.isSuper, mediaCtrl.deleteFile);
router.delete('/media/files', auth.isSuper, mediaCtrl.deleteAllFiles);

module.exports = router;