'use strict';

const express = require('express');
const router = express.Router();

// My controllers
const billCtrl = require('../../controllers/bills');

// My middlewares
const auth = require('../../middlewares/auth');

/** ---------- POST ---------- */
router.post('/', billCtrl.createBill);

/** ---------- GET ---------- */
router.get('/', auth.isBasic, billCtrl.getBills);

module.exports = router;