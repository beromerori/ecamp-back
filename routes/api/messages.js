'use strict';

const express = require('express');
const router = express.Router();

// My controllers
const messageCtrl = require('../../controllers/messages');

// My middlewares
const auth = require('../../middlewares/auth');

/** ---------- GET ---------- */
router.get('/', auth.isBasic, messageCtrl.getMessages);
router.get('/:id', auth.isBasic, messageCtrl.getMessage);

/** ---------- POST ---------- */
router.post('/', auth.isMedium, messageCtrl.createMessage);

module.exports = router;