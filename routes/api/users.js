'use strict';

const express = require('express');
const router = express.Router();

// My controllers
const mediaCtrl = require('../../controllers/media');
const userCtrl = require('../../controllers/users');

// My middlewares
const auth = require('../../middlewares/auth');

router.put('/', userCtrl.updatePasswordUser);

/** ---------- GET ---------- */
router.get('/', auth.isBasic, userCtrl.getUsers);
router.get('/participants', auth.isMedium, userCtrl.getUsersParticipants);
router.get('/profile', auth.isBasic, userCtrl.getUserByEmail);
router.get('/:id', auth.isBasic, userCtrl.getUser);
router.get('/:id/childs', auth.isBasic, userCtrl.getUserChilds);

/** ---------- POST ---------- */
router.post('/', auth.isSuper, userCtrl.createUser);
router.post('/media/files', auth.isBasic, mediaCtrl.uploadFile);

/** ---------- PUT ---------- */
router.put('/:id', auth.isBasic, userCtrl.updateUser);

/** ---------- DELETE ---------- */
router.delete('/:id', auth.isSuper, userCtrl.deleteUser);
router.delete('/media/files/:public_id', auth.isSuper, mediaCtrl.deleteFile);
router.delete('/media/files', auth.isSuper, mediaCtrl.deleteAllFiles);

module.exports = router;