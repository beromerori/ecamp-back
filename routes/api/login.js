'use strict';

const express = require('express');
const router = express.Router();

// My controllers
const loginCtrl = require('../../controllers/login');

router.post('/', loginCtrl.signIn);

module.exports = router;