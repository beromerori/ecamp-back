'use strict';

const express = require('express');
const router = express.Router();

// My controllers
const programmingCtrl = require('../../controllers/programmings');

// My middlewares
const auth = require('../../middlewares/auth');

/** ---------- GET ---------- */
router.get('/', auth.isBasic, programmingCtrl.getProgrammings);
router.get('/:id', auth.isBasic, programmingCtrl.getProgramming);

/** ---------- POST ---------- */
router.post('/', auth.isMedium, programmingCtrl.createProgramming);

/** ---------- PUT ---------- */
router.put('/:id', auth.isMedium, programmingCtrl.updateProgramming);

/** ---------- DELETE ---------- */
router.delete('/:id', auth.isMedium, programmingCtrl.deleteProgramming);

module.exports = router;