'use strict';

const express = require('express');
const router = express.Router();

// My controllers
const chartCtrl = require('../../controllers/charts');

// My middlewares
const auth = require('../../middlewares/auth');

/** ---------- GET ---------- */
router.get('/', auth.isSuper, chartCtrl.getCharts);

module.exports = router;