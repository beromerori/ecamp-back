'use strict';

// CONST
const COMMON = require('../common/constants');

// My models
const User = require('../models/user');

// My services
const jwtService = require('../services/jwtService');

function signUp(req, res) {

    const data = req.body;
    const user = new User(data);

    user.save((error, newUser) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.USER.CREATED });

        res.status(201).json(newUser);
    });
}

function signIn(req, res) {

    const email = req.body.email;
    const pass = req.body.pass;

    User.findOne({ email }).exec(function (error, user) {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.LOGIN.NOT_LOGIN });

        if (!user) return res.status(404).json({ status: 404, message: COMMON.SERVER.ERRORS.USER.NOT_FOUND });

        user.comparePass(pass, function (isMatch) {

            if (!isMatch) return res.status(400).json({ status: 400, message: COMMON.SERVER.ERRORS.LOGIN.BAD_CREDENTIALS });

            return res.status(200).send({
                status: 200,
                message: 'OK',
                token: jwtService.createJWT(user)
            });
        });
    });
}

module.exports = {
    signUp,
    signIn
}