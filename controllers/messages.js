'use strict';

// CONST
const COMMON = require('../common/constants');

// My models
const Message = require('../models/message');

/** ---------- GET ---------- */

// GET (whith filters)
function getMessages(req, res) {

    // Filters
    const parentName = req.query.parentName;
    const parent_id = req.query.parent_id;

    // Pagination
    const last_id = req.query.last_id;
    const limit = req.query.limit || COMMON.CONST.LIMIT.MESSAGES;

    let filters = {
        parentName: parentName ? { $regex: parentName, $options: 'i' } : null,
        parent_id
    };

    for (let field in filters) {
        if (!filters[field]) delete filters[field];
    }

    if (last_id) filters._id = { $gt: last_id };

    Message.findByFilters(filters, limit, (error, messages) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.MESSAGE.NOT_MESSAGES });

        res.status(200).json(messages);
    });
}

// GET (id)
function getMessage(req, res) {

    const id = req.params.message_id;

    Message.findById(id, (error, message) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.MESSAGE.NOT_MESSAGE });

        if (!message) return res.status(404).json({ status: 404, message: COMMON.SERVER.ERRORS.MESSAGE.NOT_FOUND });

        res.status(200).json(message);
    });
}

/** ---------- POST ---------- */

function createMessage(req, res) {

    const data = req.body;
    const message = new Message(data);

    message.save((error, newMessage) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.MESSAGE.CREATED });

        res.status(201).json(newMessage);
    });
}

module.exports = {
    getMessages,
    getMessage,
    createMessage
};