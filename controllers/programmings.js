'use strict';

// CONST
const COMMON = require('../common/constants');

// My models
const Programming = require('../models/programming');

/** ---------- GET ---------- */

// GET (alls)

function getProgrammings(req, res) {

    // Filters
    const filters = {
        camp_id: req.query.camp_id
    };

    Programming.find(filters, (error, programmings) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.PROGRAMMING.NOT_PROGRAMMINGS });

        res.status(200).json(programmings);
    });
}

// GET (id)
function getProgramming(req, res) {

    const id = req.params.id;

    Programming.findById(id, (error, programming) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.PROGRAMMING.NOT_PROGRAMMING });

        if (!programming) return res.status(404).json({ status: 404, message: COMMON.SERVER.ERRORS.PROGRAMMING.NOT_FOUND });

        res.status(200).json(programming);
    });
}

/** ---------- POST ---------- */
function createProgramming(req, res) {

    const data = req.body;
    const programming = new Programming(data);

    programming.save((error, newProgramming) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.PROGRAMMING.CREATED });

        res.status(201).json(newProgramming);
    });
}

/** ---------- PUT ---------- */
function updateProgramming(req, res) {

    const id = req.params.id;
    const data = req.body;

    Programming.findByIdAndUpdate(id, data, (error, updatedProgramming) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.PROGRAMMING.UPDATED });

        if (!updatedProgramming) return res.status(404).json({ status: 404, message: COMMON.SERVER.ERRORS.PROGRAMMING.NOT_FOUND });

        res.status(200).json({ message: COMMON.SERVER.MESSAGES.PROGRAMMING.UPDATED });
    });
}

/** ---------- DELETE ---------- */
function deleteProgramming(req, res) {

    const id = req.params.id;

    Programming.findByIdAndRemove(id, (error, deletedProgramming) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.PROGRAMMING.DELETED });

        if (!deletedProgramming) return res.status(404).json({ status: 404, message: COMMON.SERVER.ERRORS.PROGRAMMING.NOT_FOUND });

        res.status(200).json({ message: COMMON.SERVER.MESSAGES.PROGRAMMING.DELETED });
    });
}

module.exports = {
    getProgrammings,
    getProgramming,
    createProgramming,
    updateProgramming,
    deleteProgramming
};