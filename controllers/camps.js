'use strict';

// Moment
const moment = require('moment');

// CONST
const COMMON = require('../common/constants');

// My models
const Camp = require('../models/camp');

/** ---------- GET ---------- */

// GET (with filters)
async function getCamps(req, res) {

    let total = 0;

    // Filters

    const type = Number(req.query.type);
    const name = req.query.name;
    const ccaa = req.query.ccaa;
    const province = req.query.province;
    const numDays = Number(req.query.numDays);
    const startDate = req.query.startDate ? Number(req.query.startDate) : moment().valueOf();
    const endDate = Number(req.query.endDate);
    const minAge = Number(req.query.minAge);
    const maxAge = Number(req.query.maxAge);
    const price = parseFloat(req.query.price);
    const modalities = req.query.modalities;

    // Pagination
    const last_id = req.query.last_id;
    const limit = Number(req.query.limit);

    let filters = {
        type,
        name: name ? { $regex: name, $options: 'i' } : null,
        ccaa,
        province,
        numDays: numDays ? { $eq: numDays } : null,
        price: price ? { $lte: price } : null
    }

    filters["date.start.timestamp"] = { $gte: startDate };
    if (endDate) filters["date.end.timestamp"] = { $lte: endDate };

    if (minAge) filters["age.min"] = { $gte: minAge };
    if (maxAge) filters["age.max"] = { $lte: maxAge };

    if (modalities) {
        const ids = modalities.split(',').map(Number);
        filters.modalities = { $in: ids };
    }

    for (let field in filters) {
        if (!filters[field]) delete filters[field];
    }

    total = await getTotalCamps(filters);

    if (last_id) filters._id = { $gt: last_id };

    await Camp.findByFilters(filters, limit, (error, camps) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.CAMP.NOT_CAMPS });

        camps = camps.filter(camp => camp.numApplications < camp.numPeople);

        res.status(200).json({ total, camps });
    });
}

function getTotalCamps(filters) {

    return new Promise((resolve, reject) => {

        Camp.find(filters).exec((error, camps) => {

            if (error) return reject({ status: 500, message: COMMON.SERVER.ERRORS.CAMP.NOT_CAMPS });

            camps = camps.filter(camp => camp.numApplications < camp.numPeople);

            resolve(camps.length);
        });
    });
}

// GET (id)
function getCamp(req, res) {

    const id = req.params.id;

    Camp.findById(id, (error, camp) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.CAMP.NOT_CAMP });

        if (!camp) return res.status(404).json({ status: 404, message: COMMON.SERVER.ERRORS.CAMP.NOT_FOUND });

        res.status(200).json(camp);
    });
}

/** ---------- POST ---------- */

function createCamp(req, res) {

    const data = req.body;
    const camp = new Camp(data);

    camp.save((error, newCamp) => {

        if (error && error.code !== 11000) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.CAMP.CREATED });
        if (error && error.code === 11000) return res.status(400).json({ status: 400, message: COMMON.SERVER.ERRORS.CAMP.ALREADY_CREATED });

        res.status(201).json(newCamp);
    });
}

/** ---------- PUT ---------- */

function updateCamp(req, res) {

    const id = req.params.id;
    const data = req.body;

    Camp.findByIdAndUpdate(id, data, (error, updatedCamp) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.CAMP.UPDATED });

        if (!updatedCamp) return res.status(404).json({ status: 404, message: COMMON.SERVER.ERRORS.CAMP.NOT_FOUND });

        res.status(200).json({ message: COMMON.SERVER.MESSAGES.CAMP.UPDATED });
    });
}

/** ---------- DELETE ---------- */

function deleteCamp(req, res) {

    const id = req.params.id;

    Camp.findByIdAndRemove(id, (error, deletedCamp) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.CAMP.DELETED });

        if (!deletedCamp) return res.status(404).json({ status: 404, message: COMMON.SERVER.ERRORS.CAMP.NOT_FOUND });

        res.status(200).json({ message: COMMON.SERVER.MESSAGES.CAMP.DELETED });
    });
}

module.exports = {
    getCamps,
    getCamp,
    createCamp,
    updateCamp,
    deleteCamp
};