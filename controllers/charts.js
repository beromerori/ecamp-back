'use strict';

// Const
const COMMON = require('../common/constants');

// My models
const Camp = require('../models/camp');

/** ---------- GET ---------- */

function getCharts(req, res) {

    let dataTotalsAndByType = getTotalsAndByType();
    let dataByModalities = [];

    let byModalities = [];

    COMMON.CONST.MODALITIES.forEach(modality => {
        byModalities.push(getByModality(modality.id));
    });

    dataByModalities = Promise.all(byModalities);

    return Promise.all([dataTotalsAndByType, dataByModalities])
        .then((data) => {

            const charts = {
                totalsAndByType: data[0],
                byModalities: {
                    labels: COMMON.CONST.MODALITIES.map(modality => modality.name),
                    data: data[1]
                }
            }

            return res.status(200).json(charts)
        })
        .catch((error) => res.status(error.status).json({ status: error.status, message: COMMON.SERVER.ERRORS.CHART.NOT_CHARTS }));
}

function getTotalsAndByType() {

    return new Promise((resolve, reject) => {

        Camp.find({}, (error, camps) => {

            if (error) return reject({ status: 502, message: COMMON.SERVER.ERRORS.CAMP.NOT_CAMPS });

            let autonomousApplications = 0;
            let exchangeApplications = 0;

            let autonomousCamps = camps.filter(camp => camp.type === COMMON.CONST.CAMP_TYPES.AUTONOMOUS.type);
            let exchangeCamps = camps.filter(camp => camp.type === COMMON.CONST.CAMP_TYPES.EXCHANGES.type);

            autonomousCamps.forEach(camp => {
                autonomousApplications += camp.numApplications;
            });

            exchangeCamps.forEach(camp => {
                exchangeApplications += camp.numApplications;
            });

            resolve({
                totalCamps: camps.length,
                totalApplications: autonomousApplications + exchangeApplications,
                campsByType: {
                    labels: [COMMON.CONST.CAMP_TYPES.AUTONOMOUS.name, COMMON.CONST.CAMP_TYPES.EXCHANGES.name],
                    numCamps: [autonomousCamps.length, exchangeCamps.length],
                    numApplications: [autonomousApplications, exchangeApplications]
                },
            });
        });
    });
}

function getByModality(modalityId) {

    return new Promise((resolve, reject) => {

        Camp.find({ modalities: { $in: modalityId } }).exec((error, camps) => {

            if (error) return reject({ status: 502, message: COMMON.SERVER.ERRORS.CAMP.NOT_CAMPS });

            if (!camps) return resolve(0);

            resolve(camps.length);
        });
    });
}

module.exports = {
    getCharts
};