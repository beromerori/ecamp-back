'use strict';

// CONST
const COMMON = require('../common/constants');

// My controllers
const emailCtrl = require('./emails');

// My models
const Camp = require('../models/camp');
const User = require('../models/user');

async function createApplication(req, res) {

    const id = req.params.id;
    const dataParent = req.body.parent;
    let dataChild = req.body.child;
    const numApplications = req.body.numApplications;
    const language = req.body.language;

    try {

        let parent = await createUser(dataParent);

        if (parent.code === 11000) parent = await findUser(dataParent.dni);

        if (parent) {

            dataChild.parent_id = parent.id;
            dataChild.email = `${parent.id}-${dataChild.name}`;

            const createdChild = await createUser(dataChild);
            //let updatedParent = {};

            if (createdChild) {
                parent.childs_ids.push(createdChild.id);
            }
            else return res.status(500).json({ message: COMMON.SERVER.ERRORS.USER.CREATED });

            const updatedParent = await updateUser(parent);
            const updatedCamp = await updateCamp(id, { numApplications });
            let sendedEmail = {};

            if (!parent.pass) sendedEmail = await emailCtrl.sendEmail(parent, createdChild, language);

            return Promise.all([updatedParent, updatedCamp, sendedEmail])
                .then((success) => res.status(200).json({ message: COMMON.SERVER.MESSAGES.APPLICATION }))
                .catch((error) => res.status(error.status).json(error));
        }
    }
    catch (error) {
        return res.status(error.status).json({ status: error.status, message: error.message });
    }
}

function createUser(data) {

    const user = new User(data);

    return new Promise((resolve, reject) => {

        user.save((error, newUser) => {

            if (error && error.code !== 11000) return reject({ status: 500, message: COMMON.SERVER.ERRORS.USER.CREATED });
            if (error && error.code === 11000) return resolve({ code: 11000 });

            resolve(newUser);
        });
    });
}

function findUser(dni) {

    return new Promise((resolve, reject) => {

        User.findOne({ dni }, (error, user) => {

            if (error) return reject({ status: 500, message: COMMON.SERVER.ERRORS.USER.NOT_USER });
            if (!user) return reject({ status: 404, message: COMMON.SERVER.ERRORS.USER.NOT_FOUND });

            resolve(user);
        });
    });
}

function updateUser(parent) {

    return new Promise((resolve, reject) => {

        User.findOneAndUpdate({ _id: parent.id }, parent, (error, user) => {

            if (error) return reject({ status: 500, message: COMMON.SERVER.ERRORS.USER.NOT_USER });
            if (!user) return reject({ status: 404, message: COMMON.SERVER.ERRORS.USER.NOT_FOUND });

            resolve(user);
        });
    });
}

function updateCamp(id, data) {

    return new Promise((resolve, reject) => {

        Camp.findOneAndUpdate({ _id: id }, data, (error, updatedCamp) => {

            if (error) return reject({ status: 500, message: COMMON.SERVER.ERRORS.CAMP.UPDATED });
            if (!updatedCamp) return reject({ status: 404, message: COMMON.SERVER.ERRORS.CAMP.NOT_FOUND });

            resolve(updatedCamp);
        });
    });
}

module.exports = {
    createApplication
};