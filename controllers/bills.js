'use strict';

// CONST
const COMMON = require('../common/constants');

// My models
const Bill = require('../models/bill');

/** ---------- GET ---------- */

function getBills(req, res) {

    // Filters

    const parentDni = req.query.parentDni;
    const parentName = req.query.parentName;
    const parentSurnames = req.query.parentSurnames;
    const parentEmail = req.query.parentEmail;
    const parentPhone = req.query.parentPhone;

    // Pagination
    const last_id = req.query.last_id;
    const limit = req.query.limit || COMMON.CONST.LIMIT.BILLS;

    let filters = {};

    filters['user.dni'] = parentDni ? { $regex: parentDni, $options: 'i' } : null;
    filters['user.name'] = parentName ? { $regex: parentName, $options: 'i' } : null;
    filters['user.surnames'] = parentSurnames ? { $regex: parentSurnames, $options: 'i' } : null;
    filters['user.email'] = parentEmail;
    filters['user.phone'] = parentPhone;

    for (let field in filters) {
        if (!filters[field]) delete filters[field];
    }

    if (last_id) filters._id = { $gt: last_id };

    Bill.findByFilters(filters, limit, (error, bills) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.BILL.NOT_BILLS });

        res.status(200).json(bills);
    });
}

/** ---------- POST ---------- */

async function createBill(req, res) {

    const user = req.body.user;
    const camp = req.body.camp;

    let filter = {};
    filter['user.dni'] = user.dni;

    const userWithBills = await findBill(filter);

    if (userWithBills) {

        userWithBills.camps.push(camp);
        userWithBills.total = (userWithBills.total + camp.price).toFixed(2);

        const updatedBill = await updateBill(userWithBills);

        if (updatedBill) return res.status(200).json({ status: 500, message: COMMON.SERVER.MESSAGES.BILL.UPDATED });
        return res.status(502).json({ status: 502, message: COMMON.SERVER.ERRORS.BILL.UPDATED });
    }

    else {

        const data = {
            user,
            camps: [camp],
            total: camp.price.toFixed(2)
        }

        const newBill = await create(data);

        if (newBill) return res.status(201).json(newBill);
        else return res.status(502).json({ status: 502, message: COMMON.SERVER.ERRORS.BILL.CREATED });
    }
}

function findBill(dni) {

    return new Promise((resolve, reject) => {

        Bill.findOne(dni, (error, bill) => {

            if (error) return reject({ status: 502, message: COMMON.SERVER.ERRORS.BILL.NOT_BILL });

            resolve(bill);
        });
    });
}

function updateBill(data) {

    return new Promise((resolve, reject) => {

        Bill.findOneAndUpdate({ _id: data._id }, data, (error, updatedBill) => {

            if (error) return reject({ status: 502, message: COMMON.SERVER.ERRORS.BILL.UPDATED });

            resolve(updatedBill);
        });
    });
}

function create(data) {

    return new Promise((resolve, reject) => {

        const bill = new Bill(data);

        bill.save((error, newBill) => {

            if (error) return reject({ status: 502, message: COMMON.SERVER.ERRORS.BILL.CREATED });

            resolve(newBill);
        });
    });
}

module.exports = {
    getBills,
    createBill
};