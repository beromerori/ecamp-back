'use strict';

const bcrypt = require('bcrypt');

// CONST
const COMMON = require('../common/constants');

// My controllers
const emailCtrl = require('./emails');

// My models
const User = require('../models/user');

/** ---------- GET ---------- */

// GET (whith filters)
function getUsers(req, res) {

    // Filters

    const role = parseInt(req.query.role);
    const dni = req.query.dni;
    const name = req.query.name;
    const surnames = req.query.surnames;
    const sex = req.query.sex;
    const age = req.query.age;

    const parent_id = req.query.parent_id;
    const camp_id = req.query.camp_id;

    // Pagination
    const last_id = req.query.last_id;
    const limit = COMMON.CONST.LIMIT.USERS;

    let filters = {
        role: role ? { $eq: role } : null,
        dni,
        name: name ? { $regex: name, $options: 'i' } : null,
        surnames: surnames ? { $regex: surnames, $options: 'i' } : null,
        sex,
        age,
        parent_id
    };

    for (let field in filters) {
        if (!filters[field]) delete filters[field];
    }

    if (camp_id) filters["camp.camp_id"] = camp_id;
    if (last_id) filters._id = { $gt: last_id };

    User.findByFilters(filters, limit, (error, users) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.USER.NOT_USERS });

        res.status(200).json(users);
    });
}

function getUsersParticipants(req, res) {

    const camp_id = req.query.camp_id;

    // Pagination
    const last_id = req.query.last_id;
    const limit = COMMON.CONST.LIMIT.USERS;

    let filters = {};

    if (camp_id) filters["camp.camp_id"] = camp_id;
    if (last_id) filters._id = { $gt: last_id };

    User.findByFilters(filters, limit, (error, users) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.USER.NOT_USERS });

        res.status(200).json(users);
    });
}

// GET (id)
function getUser(req, res) {

    const id = req.params.id;

    const query = User.findById(id).select('-pass -__v');

    query.exec((error, user) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.USER.NOT_USER });

        if (!user) return res.status(404).json({ status: 404, message: COMMON.SERVER.ERRORS.USER.NOT_FOUND });

        res.status(200).json(user);
    });
}

function getUserByDni(req, res) {

    const dni = { dni: req.query.dni };

    const query = User.findOne(dni).select('-pass -__v');

    query.exec((error, user) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.USER.NOT_USER });

        if (!user) return res.status(404).json({ status: 404, message: COMMON.SERVER.ERRORS.USER.NOT_FOUND });

        res.status(200).json(user);
    });
}

function getUserByEmail(req, res) {

    const email = { email: req.query.email };

    const query = User.findOne(email).select('-pass -__v');

    query.exec((error, user) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.USER.NOT_USER });

        if (!user) return res.status(404).json({ status: 404, message: COMMON.SERVER.ERRORS.USER.NOT_FOUND });

        res.status(200).json(user);
    });
}

function getUserChilds(req, res) {

    const id = req.params.id;

    let filters = {};

    if (id) filters.parent_id = id;

    const query = User.find(filters).select('-pass -__v');

    query.exec((error, childs) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.USER.NOT_USERS });

        res.status(200).json(childs);
    });
}

/** ---------- POST ---------- */

function createUser(req, res) {

    const language = req.query.language;
    const data = req.body;
    const user = new User(data);

    user.save((error, newUser) => {

        if (error && error.code !== 11000) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.USER.CREATED });
        if (error && error.code === 11000) return res.status(400).json({ status: 400, message: COMMON.SERVER.ERRORS.USER.ALREADY_CREATED });

        if (newUser.role === COMMON.CONST.ROLES.MEDIUM) {

            return emailCtrl.sendEmail(newUser, null, language)
                .then((success) => res.status(201).json(newUser))
                .catch((error) => res.status(error.status).json(error));
        }

        return res.status(201).json(newUser)
    });
}

/** ---------- PUT ---------- */

function updateUser(req, res) {

    const data = req.body;

    if (data.pass) {
        const salt = bcrypt.genSaltSync();
        data.pass = bcrypt.hashSync(data.pass, salt);
    }

    findUserAndUpdate(data)
        .then((success) => res.status(200).json({ message: COMMON.SERVER.MESSAGES.USER.UPDATED }))
        .catch((error) => res.status(error.status).json(error))
}

function findUserAndUpdate(user) {

    return new Promise((resolve, reject) => {

        User.findOneAndUpdate({ _id: user._id }, user, (error, updatedUser) => {

            if (error && error.code !== 11000) return reject({ status: 500, message: COMMON.SERVER.ERRORS.USER.UPDATED });
            if (error && error.code === 11000) return reject({ status: 400, message: COMMON.SERVER.ERRORS.USER.ALREADY_CREATED });
            if (!updatedUser) return reject({ status: 404, message: COMMON.SERVER.ERRORS.USER.NOT_FOUND });

            resolve(updatedUser);
        });
    });
}

function updatePasswordUser(req, res) {

    const data = req.body;

    if (data.pass) {
        const salt = bcrypt.genSaltSync();
        data.pass = bcrypt.hashSync(data.pass, salt);
    }

    User.findOneAndUpdate({ email: data.email }, data, (error, updatedUser) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.USER.UPDATED });
        if (!updatedUser) return res.status(404).json({ status: 404, message: COMMON.SERVER.ERRORS.USER.NOT_FOUND });

        res.status(200).json({ message: COMMON.SERVER.MESSAGES.USER.UPDATED });
    });
}

/** ---------- DELETE ---------- */

function deleteUser(req, res) {

    const id = req.params.id;

    User.findOneAndDelete({ _id: id }, (error, deletedUser) => {

        if (error) return res.status(500).json({ status: 500, message: COMMON.SERVER.ERRORS.USER.DELETED });

        if (!deletedUser) return res.status(404).json({ status: 404, message: COMMON.SERVER.ERRORS.USER.NOT_FOUND });

        res.status(200).json({ message: COMMON.SERVER.MESSAGES.USER.DELETED })
    });
}

module.exports = {
    getUsers,
    getUsersParticipants,
    getUser,
    getUserByDni,
    getUserByEmail,
    getUserChilds,
    createUser,
    updateUser,
    updatePasswordUser,
    deleteUser
}