'use strict';

const cloudinary = require('cloudinary');
const config = require('../lib/config');

cloudinary.config(config.cloudinary);

// CONST
const COMMON = require('../common/constants');

/** ---------- UPLOAD ---------- */

function uploadFile(req, res) {

    const type = req.body.type;
    let code = req.body.code;
    const file = req.body.file;

    if (code.includes('@')) code = code.split('@')[0];

    const url = type === 'camp' ? 'ecamp/camps' : 'ecamp/users';

    cloudinary.v2.uploader.upload(file, { folder: `${url}/${code}` }, (error, result) => {

        if (error) res.status(502).json({ status: 502, message: COMMON.SERVER.ERRORS.MEDIA.NOT_MEDIA, error });

        res.status(200).json(result);
    });
}

/** ---------- DELETE ---------- */

function deleteFile(req, res) {

    const type = req.query.type;
    const public_id = req.params.public_id;

    const url = type === 'camp' ? `ecamp/camps/${public_id}` : `ecamp/users/${public_id}`;

    cloudinary.v2.api.delete_resources([url], (error, result) => {

        if (error) return res.status(502).json({ status: 502, message: COMMON.SERVER.ERRORS.MEDIA.NOT_MEDIA, error });

        if (!result) return res.status(502).json({ status: 502, message: COMMON.SERVER.ERRORS.MEDIA.NOT_FOUND });

        res.status(200).json({ file: result });
    });
}

function deleteAllFiles(req, res) {

    const type = req.query.type;
    let folder = req.query.folder;

    if (folder.includes('@')) folder = folder.split('@')[0];

    const url = type === 'camp' ? `ecamp/camps/${folder}` : `ecamp/users/${folder}`;

    cloudinary.v2.api.delete_resources_by_prefix(url, (error, result) => {

        if (error) return res.status(502).json({ status: 502, message: COMMON.SERVER.ERRORS.MEDIA.NOT_MEDIA, error });

        if (!result) return res.status(502).json({ status: 502, message: COMMON.SERVER.ERRORS.MEDIA.NOT_FOUND });

        let deletedFiles = [];

        Object.keys(result.deleted).forEach(key => {
            deletedFiles.push({ file: key });
        });

        res.status(200).json(deletedFiles);
    });
}

module.exports = {
    uploadFile,
    deleteFile,
    deleteAllFiles
}