'use strict';

const fs = require('fs');
const handlebars = require('handlebars');
const nodemailer = require('nodemailer');

// CONST
const COMMON = require('../common/constants');

const config = require('../lib/config');

function sendEmail(user, child, language) {

    const to = user.email;
    const from = 'Ecamp Info';
    const name = user.name;
    const childName = child ? child.name : '';
    const subject = language === 'en' ? `E-mail's confirmation` : `Confirmación de correo`;

    let campName = '';

    if (user.camp) campName = user.camp.name;
    else if (child.camp) campName = child.camp.name;

    // Email
    const templateCode = language === 'en' ? 'en' : 'es';
    const template = user.role === COMMON.CONST.ROLES.BASIC ? `email-parent-${templateCode}` : `email-instructor-${templateCode}`;

    const transporter = nodemailer.createTransport({
        host: config.nodemailer.host,
        port: config.nodemailer.port,
        secure: config.nodemailer.secure,
        auth: {
            user: config.nodemailer.auth.user,
            pass: config.nodemailer.auth.pass
        }
    });

    const readHTMLFile = function (path, callback) {

        fs.readFile(path, { encoding: 'utf-8' }, function (error, html) {

            if (error) return callback(error);
            else callback(null, html);
        });
    };

    return new Promise((resolve, reject) => {

        readHTMLFile(`assets/templates/${template}.html`, (error, html) => {

            if (error) return reject({ status: 500, message: COMMON.SERVER.ERRORS.EMAIL.NOT_EMAIL, error });

            const template = handlebars.compile(html);
            const replacements = { name, childName, campName, to, language };
            const htmlToSend = template(replacements);
            const mailOptions = {
                from,
                to,
                subject,
                html: htmlToSend
            };

            transporter.sendMail(mailOptions, (error, info) => {

                if (error) reject({ status: 502, message: COMMON.SERVER.ERRORS.EMAIL.NOT_EMAIL, error });

                resolve(info);
            });
        });
    });
}

module.exports = {
    sendEmail
};