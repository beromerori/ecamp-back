'use strict';

const jwtService = require('../services/jwtService');
const COMMON = require('../common/constants');

function isBasic(req, res, next) {

    if (!req.headers.authorization) return res.status(401).send({ status: 401, message: COMMON.SERVER.ERRORS.AUTHORIZATION.NOT_AUTHORIZED });

    const token = req.headers.authorization.split(' ')[1];

    jwtService.decodeJWT(token)
        .then(function (response) {

            const role = response;
            return role && role >= COMMON.CONST.ROLES.BASIC ? next() : res.status(403).send({ status: 403, message: COMMON.SERVER.ERRORS.AUTHORIZATION.NOT_PRIVILEGES });
        })
        .catch(function (error) {
            return res.status(error.status).send(error);
        });
}

function isMedium(req, res, next) {

    if (!req.headers.authorization) return res.status(401).send({ status: 401, message: COMMON.SERVER.ERRORS.AUTHORIZATION.NOT_AUTHORIZED });

    const token = req.headers.authorization.split(' ')[1];

    jwtService.decodeJWT(token)
        .then(function (response) {

            const role = response;
            return role && role >= COMMON.CONST.ROLES.MEDIUM ? next() : res.status(403).send({ status: 403, message: COMMON.SERVER.ERRORS.AUTHORIZATION.NOT_PRIVILEGES });
        })
        .catch(function (error) {
            return res.status(error.status).send(error);
        });
}

function isSuper(req, res, next) {

    if (!req.headers.authorization) return res.status(401).send({ status: 401, message: COMMON.SERVER.ERRORS.AUTHORIZATION.NOT_AUTHORIZED });

    const token = req.headers.authorization.split(' ')[1];

    jwtService.decodeJWT(token)
        .then(function (response) {

            const role = response;
            return role && role === COMMON.CONST.ROLES.SUPER ? next() : res.status(403).send({ status: 403, message: COMMON.SERVER.ERRORS.AUTHORIZATION.NOT_PRIVILEGES });
        })
        .catch(function (error) {
            return res.status(error.status).send(error);
        });
}

module.exports = {
    isBasic,
    isMedium,
    isSuper
};