'use strict';

const CONST = {
    CAMP_TYPES: {
        AUTONOMOUS: {
            name: 'CAMPS.TYPE.AUTONOMOUS',
            type: 1
        },
        EXCHANGES: {
            name: 'CAMPS.TYPE.EXCHANGES',
            type: 2
        }
    },
    LIMIT: {
        BILLS: 10,
        CAMPS: 10,
        MESSAGES: 10,
        PROGRAMMINGS: 10,
        RAFFLES: 10,
        USERS: 10,
    },
    MODALITIES: [
        {
            id: 0,
            name: 'CAMPS.MODALITIES.CIRCUS'
        },
        {
            id: 1,
            name: 'CAMPS.MODALITIES.CLIMBING'
        },
        {
            id: 2,
            name: 'CAMPS.MODALITIES.CREATIVE'
        },
        {
            id: 3,
            name: 'CAMPS.MODALITIES.CULTURAL'
        },
        {
            id: 4,
            name: 'CAMPS.MODALITIES.ENGLISH'
        },
        {
            id: 5,
            name: 'CAMPS.MODALITIES.ENVIRONMENTAL'
        },
        {
            id: 6,
            name: 'CAMPS.MODALITIES.FRESH-AIR'
        },
        {
            id: 7,
            name: 'CAMPS.MODALITIES.GAMES'
        },
        {
            id: 8,
            name: 'CAMPS.MODALITIES.MULTI-ADVENTURE'
        },
        {
            id: 9,
            name: 'CAMPS.MODALITIES.MUSICAL'
        },
        {
            id: 10,
            name: 'CAMPS.MODALITIES.NAUTICAL'
        },
        {
            id: 11,
            name: 'CAMPS.MODALITIES.PHOTOGRAPHY'
        },
        {
            id: 12,
            name: 'CAMPS.MODALITIES.SCIENTIFIC'
        },
        {
            id: 13,
            name: 'CAMPS.MODALITIES.SPORTS-PRACTICES'
        },
        {
            id: 14,
            name: 'CAMPS.MODALITIES.THEATER'
        },
        {
            id: 15,
            name: 'CAMPS.MODALITIES.TREKKING'
        }
    ],
    NUM_RAFFLE: 1,
    ROLES: {
        BASIC: 1,
        MEDIUM: 2,
        SUPER: 3
    }
};

const SERVER = {

    ERRORS: {
        APPLICATION: {
            NOT_APPLICATION: 'SERVER.ERRORS.APPLICATION.NOT-APPLICATION'
        },
        AUTHORIZATION: {
            BAD_CREDENTIALS: 'SERVER.ERRORS.AUTHORIZATION.BAD-CREDENTIALS',
            NOT_AUTHORIZED: 'SERVER.ERRORS.AUTHORIZATION.NOT-AUTHORIZED',
            NOT_LOGIN: 'SERVER.ERRORS.AUTHORIZATION.NOT-LOGIN',
            NOT_PRIVILEGES: 'SERVER.ERRORS.AUTHORIZATION.NOT-PRIVILEGES',
            TOKEN_EXPIRED: 'SERVER.ERRORS.AUTHORIZATION.TOKEN-EXPIRED',
            TOKEN_INVALID: 'SERVER.ERRORS.AUTHORIZATION.TOKEN-INVALID'
        },
        BILL: {
            CREATED: 'SERVER.ERRORS.BILL.CREATED',
            NOT_BILL: 'SERVER.ERRORS.BILL.NOT-BILL',
            NOT_BILLS: 'SERVER.ERRORS.BILL.NOT-BILLS',
            NOT_FOUND: 'SERVER.ERRORS.BILL.NOT-FOUND',
            UPDATED: 'SERVER.ERRORS.BILL.UPDATED'
        },
        CAMP: {
            ALREADY_CREATED: 'SERVER.ERRORS.CAMP.ALREADY-CREATED',
            CREATED: 'SERVER.ERRORS.CAMP.CREATED',
            DELETED: 'SERVER.ERRORS.CAMP.DELETED',
            NOT_CAMP: 'SERVER.ERRORS.CAMP.NOT-CAMP',
            NOT_CAMPS: 'SERVER.ERRORS.CAMP.NOT-CAMPS',
            NOT_FOUND: 'SERVER.ERRORS.CAMP.NOT-FOUND',
            UPDATED: 'SERVER.ERRORS.CAMP.UPDATED'
        },
        CHART: {
            NOT_CHARTS: 'SERVER.ERRORS.CHART.NOT-CHARTS'
        },
        EMAIL: {
            NOT_EMAIL: 'SERVER.ERRORS.EMAIL.NOT-EMAIL'
        },
        LOGIN: {
            BAD_CREDENTIALS: 'SERVER.ERRORS.LOGIN.BAD-CREDENTIALS',
            NOT_LOGIN: 'SERVER.ERRORS.LOGIN.NOT-LOGIN'
        },
        MEDIA: {
            NOT_FOUND: 'SERVER.ERRORS.MEDIA.NOT-FOUND',
            NOT_MEDIA: 'SERVER.ERRORS.MEDIA.NOT-MEDIA'
        },
        MESSAGE: {
            CREATED: 'SERVER.ERRORS.MESSAGE.CREATED',
            NOT_FOUND: 'SERVER.ERRORS.MESSAGE.NOT-FOUND',
            NOT_MESSAGE: 'SERVER.ERRORS.MESSAGE.NOT-MESSAGE',
            NOT_MESSAGES: 'SERVER.ERRORS.MESSAGE.NOT-MESSAGES',
        },
        PAYMENT: {
            NOT_PAYMENT: 'SERVER.ERRORS.PAYMENT.NOT-PAYMENT'
        },
        PROGRAMMING: {
            CREATED: 'SERVER.ERRORS.PROGRAMMING.CREATED',
            DELETED: 'SERVER.ERRORS.PROGRAMMING.DELETED',
            NOT_FOUND: 'SERVER.ERRORS.PROGRAMMING.NOT-FOUND',
            NOT_PROGRAMMING: 'SERVER.ERRORS.PROGRAMMING.NOT-PROGRAMMING',
            NOT_PROGRAMMINGS: 'SERVER.ERRORS.PROGRAMMING.NOT-PROGRAMMINGS',
            UPDATED: 'SERVER.ERRORS.PROGRAMMING.UPDATED'
        },
        USER: {
            ALREADY_CREATED: 'SERVER.ERRORS.USER.ALREADY-CREATED',
            CREATED: 'SERVER.ERRORS.USER.CREATED',
            DELETED: 'SERVER.ERRORS.USER.DELETED',
            NEW_PASSWORD: 'SERVER.ERRORS.USER.NEW-PASSWORD',
            NOT_FOUND: 'SERVER.ERRORS.USER.NOT-FOUND',
            NOT_USER: 'SERVER.ERRORS.USER.NOT-USER',
            NOT_USERS: 'SERVER.ERRORS.USER.NOT-USERS',
            UPDATED: 'SERVER.ERRORS.USER.UPDATED'
        }
    },
    MESSAGES: {
        APPLICATION: 'SERVER.MESSAGES.APPLICATION',
        BILL: {
            CREATED: 'SERVER.MESSAGES.BILL.CREATED'
        },
        CAMP: {
            CREATED: 'SERVER.MESSAGES.CAMP.CREATED',
            DELETED: 'SERVER.MESSAGES.CAMP.DELETED',
            UPDATED: 'SERVER.MESSAGES.CAMP.UPDATED'
        },
        MESSAGE: {
            CREATED: 'SERVER.MESSAGES.MESSAGE.CREATED',
        },
        PAYMENT: 'SERVER.MESSAGES.PAYMENT',
        PROGRAMMING: {
            CREATED: 'SERVER.MESSAGES.PROGRAMMING.CREATED',
            DELETED: 'SERVER.MESSAGES.PROGRAMMING.DELETED',
            UPDATED: 'SERVER.MESSAGES.PROGRAMMING.UPDATED'
        },
        USER: {
            CREATED: 'SERVER.MESSAGES.USER.CREATED',
            DELETED: 'SERVER.MESSAGES.USER.DELETED',
            NEW_PASSWORD: 'SERVER.MESSAGES.USER.NEW-PASSWORD',
            UPDATED: 'SERVER.MESSAGES.USER.UPDATED'
        }
    }
};

module.exports = {
    CONST,
    SERVER
}