'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ChartSchema = new Schema({

    totalsAndByType: {
        totalCamps: Number,
        totalApplications: Number,
        campsByType: {
            labels: [String],
            numCamps: [Number],
            numApplications: [Number]
        }
    },
    byModalities: {
        labels: [String],
        data: [Number]
    }
});

const Chart = mongoose.model('Chart', ChartSchema);

module.exports = mongoose.model('Chart', ChartSchema);