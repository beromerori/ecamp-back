'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProgrammingSchema = new Schema({

    title: String,
    date: {
        timestamp: Number,
        time: String
    },
    body: String,
    images: [
        {
            alt: String,
            bytes: Number,
            created_at: String,
            etag: String,
            format: String,
            height: Number,
            placeholder: Boolean,
            public_id: String,
            resource_type: String,
            secure_url: String,
            signature: String,
            tags: [String],
            url: String,
            version: Number,
            width: Number
        }
    ],
    camp_id: {
        type: String,
        ref: 'Camp'
    }
});

const Programming = mongoose.model('Programming', ProgrammingSchema);

module.exports = mongoose.model('Programming', ProgrammingSchema);