'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MessageSchema = new Schema({

    title: String,
    body: String,
    date: {
        timestamp: Number,
        time: String
    },
    parent_id: {
        type: String,
        ref: 'User'
    }
});

MessageSchema.statics.findByFilters = function (filters, limit, callback) {

    const query = Message.find(filters);
    query.exec(callback);
}

const Message = mongoose.model('Message', MessageSchema);

module.exports = mongoose.model('Message', MessageSchema);