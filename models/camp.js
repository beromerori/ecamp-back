'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CampSchema = new Schema({
    type: Number,
    code: {
        type: String,
        unique: true
    },
    name: String,
    description: String,
    images: [
        {
            alt: String,
            bytes: Number,
            created_at: String,
            etag: String,
            format: String,
            height: Number,
            placeholder: Boolean,
            public_id: String,
            resource_type: String,
            secure_url: String,
            signature: String,
            tags: [String],
            url: String,
            version: Number,
            width: Number
        }
    ],
    pdf: {
        type: {
            alt: String,
            bytes: Number,
            created_at: String,
            etag: String,
            format: String,
            height: Number,
            placeholder: Boolean,
            public_id: String,
            resource_type: String,
            secure_url: String,
            signature: String,
            tags: [String],
            url: String,
            version: Number,
            width: Number
        },
        required: false
    },
    ccaa: Number,
    province: String,
    cp: Number,
    address: String,
    installations: String,
    latitude: Number,
    longitude: Number,
    date: {
        start: {
            timestamp: Number,
            time: String
        },
        end: {
            timestamp: Number,
            time: String
        }
    },
    status: Number,
    numDays: Number,
    numPeople: Number,
    numApplications: Number,
    age: {
        min: Number,
        max: Number
    },
    price: Number,
    modalities: [{
        type: Number
    }],
    observations: {
        type: String,
        required: false
    }
});

CampSchema.statics.findByFilters = function (filters, limit, callback) {

    const query = Camp.find(filters).limit(limit).sort({ '_id': 1 });
    query.exec(callback);
}

var Camp = mongoose.model('Camp', CampSchema);

module.exports = mongoose.model('Camp', CampSchema);