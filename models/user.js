'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bcrypt = require('bcrypt');

var UserSchema = new Schema({
    role: Number,
    avatar: {
        type: {
            alt: String,
            bytes: Number,
            created_at: String,
            etag: String,
            format: String,
            height: Number,
            placeholder: Boolean,
            public_id: String,
            resource_type: String,
            secure_url: String,
            signature: String,
            tags: [String],
            url: String,
            version: Number,
            width: Number
        },
        required: false
    },
    dni: {
        type: String,
        required: false
    },
    name: String,
    surnames: String,
    sex: String,
    age: Number,
    birthday: {
        timestamp: Number,
        time: String
    },
    phone: {
        type: String,
        required: false
    },
    email: {
        type: String,
        unique: true,
        required: false
    },
    pass: {
        type: String,
        required: false
    },
    address: String,
    cp: Number,
    location: String,
    province: String,
    // INSTRUCTOR
    camp: {
        type: {
            camp_id: {
                type: String,
                ref: 'Camp',
            },
            campType: Number,
            code: String,
            name: String
        },
        required: false
    },
    cv: {
        type: {
            alt: String,
            bytes: Number,
            created_at: String,
            etag: String,
            format: String,
            height: Number,
            placeholder: Boolean,
            public_id: String,
            resource_type: String,
            secure_url: String,
            signature: String,
            tags: [String],
            url: String,
            version: Number,
            width: Number
        },
        required: false
    },
    // PARENT
    childs_ids: [{
        type: String,
        ref: 'User',
        required: false
    }],
    bonus: {
        type: String,
        required: false
    },
    // CHILD
    parent_id: {
        type: String,
        ref: 'User',
        required: false
    },
    intolerances: {
        type: String,
        required: false
    }
});

UserSchema.pre('save', function (next) {

    let u = this;

    if (!u.pass) return next();

    if (!u.isNew && !u.isModified('pass')) return next();

    const salt = bcrypt.genSaltSync();
    u.pass = bcrypt.hashSync(u.pass, salt);
    next();
});

UserSchema.methods.comparePass = function (password, callback) {
    return callback(bcrypt.compareSync(password, this.pass));
}

/*UserSchema.methods.findByFilters = function (filters, limit, callback) {

    var query = User.find(filters).limit(limit);
    query.exec(callback);
}*/

UserSchema.statics.findByFilters = function (filters, limit, callback) {

    var query = User.find(filters).select('-pass -__v');
    query.exec(callback);
}

var User = mongoose.model('User', UserSchema);

module.exports = mongoose.model('User', UserSchema);