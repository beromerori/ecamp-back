'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BillSchema = new Schema({

    user: {
        dni: String,
        name: String,
        surnames: String,
        email: String,
        phone: String,
    },
    camps: [
        {
            camp_id: {
                type: String,
                ref: 'Camp',
            },
            campType: Number,
            code: String,
            name: String,
            price: Number
        }
    ],
    total: Number
});

BillSchema.statics.findByFilters = function (filters, limit, callback) {

    const query = Bill.find(filters);
    query.exec(callback);
}

const Bill = mongoose.model('Bill', BillSchema);

module.exports = mongoose.model('Bill', BillSchema);