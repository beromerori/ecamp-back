'use strict';

const mongoose = require('mongoose');
const config = require('./config');

/*mongoose.connect(config.db, function (err) {

    if (err) {
        console.log('Failed connection');
        return;
    }

    console.log('Connected to mongoDB');
});*/

mongoose.set('useNewUrlParser', true);
mongoose.set('useCreateIndex', true);

mongoose.connect(config.db).then(
    function (success) {
        console.log('Connected to mongoDB');
    },
    function (err) {
        console.log('Failed connection to mongoDB');
        return;
    }
);